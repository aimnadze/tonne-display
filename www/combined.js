(function () {
function BuildQuery (params) {

    function scan (params, prefix) {
        for (const i in params) {
            const value = (() => {
                const value = params[i]
                if (typeof value === 'boolean') return value ? '1' : ''
                return value
            })()
            if (typeof value === 'object') {
                scan(value, prefix + i + '_')
                continue
            }
            if (value === null) continue
            flat[prefix + i] = value
        }
    }

    const flat = {}
    scan(params, '', '')

    const array = []
    for (const i in flat) {
        array.push(i + '=' + encodeURIComponent(flat[i]))
    }
    return array.join('&')

}
;
function CollapseSpaces (string) {
    return string.replace(/\s+/g, ' ').replace(/^\s*(.*?)\s*$/, '$1')
}
;
function Dialog (options) {
    const element = Element({
        className: 'Dialog',
        onclick (e) {
            if (e.target === element) options.close()
        },
    }, [
        Element({ className: 'Dialog-content' }, options.content),
    ])
    return element
}
;
function Element (...args) {

    const name = typeof args[0] === 'string' ? args.shift() : 'div'
    const properties = args[0] instanceof Array ? {} : args.shift()
    const content = args[0] instanceof Array ? args.shift() : []

    const element = document.createElement(name)
    for (const i in properties) {
        if (i === 'style') {
            Object.assign(element.style, properties[i])
            continue
        }
        element[i] = properties[i]
    }
    element.append(...content)
    return element

}
;
function Event (options) {

    function listen (listener) {
        if (typeof listener !== 'function') throw new Error
        if (!options.unlimited && listeners.length === 16) {
            throw new Error('Too many listeners')
        }
        listeners.push(listener)
    }

    function unlisten (listener) {
        const index = listeners.indexOf(listener)
        if (index === -1) throw new Error
        listeners.splice(index, 1)
    }

    if (options === undefined) options = {}

    const listeners = []

    return {
        listen, unlisten,
        emit (...args) {
            listeners.slice(0).forEach(listener => {
                listener(...args)
            })
        },
        reset () {
            listeners.splice(0)
        },
    }

}
;
function Form (options) {

    let abort = () => {}

    let error_element = null

    const element = Element('form', {
        onsubmit (e) {

            abort()
            e.preventDefault()

            const params = options.submit()
            if (params === undefined) return

            abort = Post({
                url: 'api/' + options.action,
                body: BuildQuery(params),
                error () {},
                load (response) {

                    if (response === 'LOG_IN') {
                        options.logout()
                        return
                    }

                    options.done(response, params)

                },
            })

        },
    }, [
        Page_H1({ text: options.title }),
        ...options.items,
    ])

    return {
        element,
        destroy () {
            abort()
        },
        error (text) {
            error_element = Form_Error({ text: text })
            element.insertBefore(error_element, element.childNodes[1])
            abort = () => {
                element.removeChild(error_element)
                abort = () => {}
            }
        },
    }

}
;
function Form_Button (options) {
    return Element('button', { className: 'Form_Button' }, [options.text])
}
;
function Form_Error (options) {
    return Element({ className: 'Form_Error' }, [options.text])
}
;
function Form_TextInput (options) {

    const input = Element('input', {
        className: 'Form_TextInput-input',
        name: options.name,
    })
    if (options.type !== undefined) input.type = options.type
    if (options.value !== undefined) input.value = options.value

    return {
        element: Element({
            className: 'Form_TextInput' + (options.next ? ' Next-medium' : ''),
        }, [options.label + ':', input]),
        focus () {
            input.focus()
        },
        get_value () {
            const value = (() => {
                const value = input.value
                if (options.type === 'password') return value
                return CollapseSpaces(value)
            })()
            if (options.required && value === '') {
                input.value = ''
                input.focus()
                return null
            }
            return value
        },
    }

}
;
function FormatTime (time) {
    const date = new Date(time)
    return MonthNameShort[date.getUTCMonth()] + ' ' +
        TwoDigitPad(date.getUTCDate()) + ' ' +
        TwoDigitPad(date.getUTCHours()) + ':' +
        TwoDigitPad(date.getUTCMinutes())
}
;
function GraphCeil (n) {

    const steps = [1, 2, 4, 5]
    let multiplier = 1

    while (true) {
        for (let i = 0; i < steps.length; i++) {
            const snap = steps[i] * multiplier
            if (n <= snap) return snap
        }
        multiplier *= 10
    }

}
;
function GraphCeilBytes (n) {
    return Math.pow(2, Math.ceil(Math.log(n) / Math.log(2)))
}
;
function GraphSteps (n) {

    let divider = 1
    while (true) {
        if (n <= 5) break
        n /= 10
        divider *= 10
    }

    const steps = (() => {
        if (n <= 1) return [1, 0.8, 0.6, 0.4, 0.2]
        else if (n <= 2) return [2, 1.5, 1, 0.5]
        else if (n <= 4) return [4, 3, 2, 1]
        else return [5, 4, 3, 2, 1]
    })()

    for (let i = 0; i < steps.length; i++) {
        steps[i] *= divider
    }

    return steps

}
;
function GraphStepsBytes (n) {
    const precision = Math.pow(10, 5)
    const power = Math.round(Math.log(n) / Math.log(2) * precision) / precision
    const ceil = Math.pow(2, Math.ceil(power))
    return [ceil, ceil * 3 / 4, ceil / 2, ceil / 4]
}
;
function HomePage (application) {

    function logout () {
        application.load(LoginPage_Main)
    }

    let period = null

    let abort = () => {}

    const top = Top(application, {
        logout () {
            abort()
            logout()
        },
    })

    const element = Element([
        top.element,
        PeriodSelect_Main(application, {
            logout () {
                top.destroy()
                logout()
            },
            update (filter) {
                abort()
                abort = Period_Main({
                    filter, logout,
                    done (new_period) {
                        const scrollTop = element.scrollTop
                        if (period !== null) period.remove()
                        period = new_period
                        element.append(period)
                        element.scrollTop = scrollTop
                    },
                })
            },
        }),
    ])

    return element

}
;
function LoginPage_Form (application) {

    function error (text) {
        error_element = Form_Error({ text: text })
        form.insertBefore(error_element, username_input.element)
    }

    let abort = () => {}

    let error_element = null

    const username_input = Form_TextInput({
        name: 'username',
        label: 'Username',
        required: true,
    })

    const password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'password',
        label: 'Password',
        required: true,
    })

    const form = Element('form', {
        className: 'LoginPage_Form',
        onsubmit (e) {

            abort()
            e.preventDefault()

            const username = username_input.get_value()
            if (username === null) return

            const password = password_input.get_value()
            if (password === null) return

            abort = Post({
                url: 'api/login/',
                body: BuildQuery({ username, password }),
                error () {},
                load (response) {

                    if (response === true) {
                        application.variables.session_user = {
                            username: username,
                        }
                        application.load(HomePage)
                        return
                    }

                    if (response === 'LOGIN_INVALID') {
                        error('Invalid username or password')
                        abort = () => {
                            form.removeChild(error_element)
                        }
                        return
                    }

                },
            })

        },
    }, [
        Element({ className: 'LoginPage_Form-icon' }, [
            Element({
                className: 'LoginPage_Form-icon-content',
                style: { backgroundImage: 'url(img/icon/32.svg)' },
            }),
        ]),
        Element([
            Element({ className: 'LoginPage_Form-title' }, ['Ton']),
        ]),
        Element({ className: 'LoginPage_Form-subtitle' }, [
            'Load Monitoring',
        ]),
        username_input.element,
        password_input.element,
        Form_Button({ text: 'Log in' }),
    ])

    application.event.load.listen(username_input.focus)

    return form

}
;
function LoginPage_Main (application) {
    return Element({
        className: 'LoginPage_Main',
        style: { backgroundImage: 'url(img/login.svg)' },
    }, [
        Element({ className: 'LoginPage_Main-content' }, [
            LoginPage_Form(application),
        ]),
    ])
}
;
window.Main = variables => {

    function load (page) {
        current_page = page(application)
        element.append(current_page)
        event.load.emit()
    }

    let current_page
    const time_difference = Date.now() - variables.time

    const event = {
        load: Event(),
        scroll: Event(),
    }

    const application = {
        event, variables,
        load (page) {
            current_page.remove()
            event.load.reset()
            event.scroll.reset()
            load(page)
        },
        resource_url (path) {
            return variables.site_base + path
        },
        show_form (form) {

            function hide () {
                element.removeChild(dialog)
            }

            const dialog = Dialog({
                content: [form.element],
                close () {
                    hide()
                    form.destroy()
                },
            })
            element.append(dialog)
            form.focus()
            return hide

        },
        time_now () {
            return Date.now() - time_difference
        },
    }

    const element = Element({
        className: 'Main',
        onscroll () {
            event.scroll.emit(element.scrollTop)
        },
    })

    document.body.append(element)
    load(variables.session_user ? HomePage : LoginPage_Main)

}
;
const MonthNameShort = [
    'Jan', 'Feb', 'Mar',
    'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep',
    'Oct', 'Nov', 'Dec',
]
;
function Page_H1 (options) {
    return Element('h1', { className: 'Page_H1' }, [options.text])
}
;
function PasswordForm (options) {

    const current_password_input = Form_TextInput({
        type: 'password',
        name: 'current_password',
        label: 'Current password',
        required: true,
    })

    const new_password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'new_password',
        label: 'New password',
        required: true,
    })

    const repeat_new_password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'repeat_new_password',
        label: 'Repeat new password',
        required: true,
    })

    const form = Form({
        title: 'Change password',
        action: 'password/',
        logout: options.logout,
        items: [
            current_password_input.element,
            new_password_input.element,
            repeat_new_password_input.element,
            Form_Button({ text: 'Save' }),
        ],
        submit () {

            const current_password = current_password_input.get_value()
            if (current_password === null) return

            const new_password = new_password_input.get_value()
            if (new_password === null) return

            const repeat_new_password = repeat_new_password_input.get_value()
            if (repeat_new_password === null) return

            if (new_password !== repeat_new_password) {
                form.error('New passwords doesn\'t match')
                return
            }

            return {
                current_password: current_password,
                new_password: new_password,
            }

        },
        done (response) {

            if (response === true) {
                options.done()
                return
            }

            if (response === 'CURRENT_PASSWORD_INVALID') {
                form.error('Invalid current password')
                return
            }

        },
    })

    return {
        destroy: form.destroy,
        element: form.element,
        focus: current_password_input.focus,
    }

}
;
function Period_Chart (options) {

    const filter = options.filter

    const width_ratio = (() => {
        let width_ratio = (1000 * 60) / (filter.to - filter.from)
        while (width_ratio < 0.005) width_ratio *= 2
        return width_ratio
    })()

    const rendered_bands = options.bands.map(band => ({
        band: band,
        rendered_values: options.items.map(band.render),
    }))

    const max_value = (() => {
        let max_value = 1
        rendered_bands.forEach(band => {
            band.rendered_values.forEach(value => {
                max_value = Math.max(max_value, value)
            })
        })
        return options.ceil(max_value)
    })()

    const format = options.format(max_value)

    return Element({ className: 'Period_Chart' }, [
        Element({ className: 'Period_Chart-title' }, [options.title]),
        Element({ className: 'Period_Chart-image' }, [
            Element({
                className: 'Period_Chart-image-items',
            }, rendered_bands.map(band => (
                options.items.map((item, i) => {

                    const time = item.time
                    const value = band.rendered_values[i]
                    const left_ratio = (time - filter.from / 1000) / ((filter.to - filter.from) / 1000)
                    const height_ratio = value / max_value

                    return Element({
                        className: 'Period_Chart-image-item',
                        style: {
                            backgroundColor: band.band.color,
                            left: (left_ratio * 100) + '%',
                            width: 'calc(' + (width_ratio * 100) + '% + 1px)',
                            height: (height_ratio * 100) + '%',
                        },
                    })

                })
            )).flat()),
            Element({ className: 'Period_Chart-image-lines' }, [
                ...options.steps(max_value).map((value, index) => {

                    const percent = Math.round(value / max_value * 100) + '%'

                    return Element({
                        className: 'Period_Chart-image-line' + (index === 0 ? ' first' : ''),
                        style: { height: percent } ,
                    }, [
                        Element({
                            className: 'Period_Chart-image-line-value',
                        }, [format(value)]),
                        Element({
                            className: 'Period_Chart-image-line-percent',
                        }, [percent]),
                    ])

                }),
                Element({ className: 'Period_Chart-image-lines-zero' }),
            ]),
            Element({ className: 'Period_Chart-image-border' }),
        ]),
    ])

}
;
function Period_IO (filter, items) {
    return Period_TwinChart({
        title: 'Disk Read / Write',
        filter: filter,
        items: items,
        ceil: GraphCeilBytes,
        steps: GraphStepsBytes,
        bands: [{
            color: 'hsl(140, 50%, 55%)',
            render (item) {
                return item.io.read
            },
        }, {
            color: 'hsl(320, 50%, 65%)',
            render (item) {
                return -item.io.write
            },
        }],
        format (max_value) {

            function apply (_unit) {
                max_value /= 1024
                divider *= 1024
                unit = _unit
            }

            let unit = 'B/s'
            let divider = 1
            if (max_value >= 1024) apply('KB/s')
            if (max_value >= 1024) apply('MB/s')

            return value => (
                (value / divider).toFixed(1).replace(/\.?0+$/g, '') + ' ' + unit
            )

        },
    })
}
;
function Period_Item (filter, agent, items) {

    function item (element, className) {
        return Element({
            className: 'Period_Item-item' + (className === undefined ? '' : ' ' + className),
        }, [element])
    }

    return Element({ className: 'Period_Item' }, [
        Element('h2', { className: 'Period_Item-title' }, [agent.name]),
        Element({ className: 'Period_Item-content' }, [
            item(Period_Processor(filter, items), 'processor'),
            item(Period_Memory(filter, items)),
            item(Period_Storage(filter, items)),
            item(Period_Network(filter, items), 'network'),
            item(Period_IO(filter, items), 'io'),
        ]),
    ])

}
;
function Period_Main (options) {

    const filter = options.filter

    return Post({
        url: 'api/fetch/',
        body: BuildQuery({
            from: Math.floor(filter.from / 1000),
            to: Math.floor(filter.to / 1000),
        }),
        error () {},
        load (response) {

            if (response === 'LOG_IN') {
                options.logout()
                return
            }

            const agent_map_items = {}
            response.agents.forEach(agent => {
                agent_map_items[agent.id] = []
            })
            response.reads.forEach(item => {
                const items = agent_map_items[item.agent_id]
                if (items === undefined) return
                items.push(item)
            })

            options.done(Element({
                className: 'Period_Main',
            }, response.agents.map(agent => (
                Period_Item(filter, agent, agent_map_items[agent.id])
            ))))

        },
    })

}
;
function Period_Memory (filter, items) {
    return Period_TwinChart({
        title: 'Memory / Swap',
        filter: filter,
        items: items,
        ceil: GraphCeilBytes,
        steps: GraphStepsBytes,
        bands: [{
            color: 'hsl(180, 50%, 85%)',
            render (item) {
                return item.os.totalmem
            },
        }, {
            color: 'hsl(140, 50%, 55%)',
            render (item) {
                return item.os.totalmem - item.os.freemem
            },
        }, {
            title: '1m',
            color: 'hsl(320, 50%, 85%)',
            render (item) {
                return -item.swap.total * 1024
            },
        }, {
            title: '15m',
            color: 'hsl(280, 50%, 65%)',
            render (item) {
                return -item.swap.used * 1024
            },
        }],
        format (max_value) {

            function apply (_unit) {
                max_value /= 1024
                divider *= 1024
                unit = _unit
            }

            let unit = 'B'
            let divider = 1
            if (max_value >= 1024) apply('KB')
            if (max_value >= 1024) apply('MB')
            if (max_value >= 1024) apply('GB')
            if (max_value >= 1024) apply('TB')

            return value => (
                (value / divider).toFixed(1).replace(/\.?0+$/g, '') + ' ' + unit
            )

        },
    })
}
;
function Period_Network (filter, items) {
    return Period_TwinChart({
        title: 'Network In / Out',
        filter: filter,
        items: items,
        ceil: GraphCeilBytes,
        steps: GraphStepsBytes,
        bands: [{
            title: '15m',
            color: 'hsl(140, 50%, 55%)',
            render (item) {
                return item.network.receive
            },
        }, {
            title: '1m',
            color: 'hsl(320, 50%, 65%)',
            render (item) {
                return -item.network.send
            },
        }],
        format (max_value) {

            function apply (_unit) {
                max_value /= 1024
                divider *= 1024
                unit = _unit
            }

            let unit = 'B/s'
            let divider = 1
            if (max_value >= 1024) apply('KB/s')
            if (max_value >= 1024) apply('MB/s')
            if (max_value >= 1024) apply('GB/s')

            return value => (
                (value / divider).toFixed(1).replace(/\.?0+$/g, '') + ' ' + unit
            )

        },
    })
}
;
function Period_Processor (filter, items) {
    return Period_Chart({
        title: 'Processor',
        filter: filter,
        items: items,
        ceil: GraphCeil,
        steps: GraphSteps,
        bands: [{
            color: 'hsl(180, 50%, 85%)',
            render (item) {
                return item.os.num_cpus
            },
        }, {
            color: 'hsl(140, 50%, 55%)',
            render (item) {
                return item.os.loadavg[0]
            },
        }],
        format () {
            return value => value
        },
    })
}
;
function Period_Storage (filter, items) {
    return Period_Chart({
        title: 'Storage',
        filter: filter,
        items: items,
        ceil: GraphCeilBytes,
        steps: GraphStepsBytes,
        bands: [{
            color: 'hsl(180, 50%, 85%)',
            render (item) {
                return item.storage.total
            },
        }, {
            color: 'hsl(140, 50%, 55%)',
            render (item) {
                return item.storage.used
            },
        }],
        format (max_value) {

            function apply (_unit) {
                max_value /= 1024
                divider *= 1024
                unit = _unit
            }

            let unit = 'KB'
            let divider = 1
            if (max_value >= 1024) apply('MB')
            if (max_value >= 1024) apply('GB')
            if (max_value >= 1024) apply('TB')

            return value => (
                (value / divider).toFixed(1).replace(/\.?0+$/g, '') + ' ' + unit
            )

        },
    })
}
;
function Period_TwinChart (options) {

    const filter = options.filter

    const width_ratio = (() => {
        let width_ratio = (1000 * 60) / (filter.to - filter.from)
        while (width_ratio < 0.005) width_ratio *= 2
        return width_ratio
    })()

    const rendered_bands = options.bands.map(band => ({
        band: band,
        rendered_values: options.items.map(band.render),
    }))

    const max_value = (() => {
        let max_value = 1
        rendered_bands.forEach(band => {
            band.rendered_values.forEach(value => {
                max_value = Math.max(max_value, Math.abs(value))
            })
        })
        return options.ceil(max_value)
    })()

    const format = options.format(max_value)

    return Element({ className: 'Period_TwinChart' }, [
        Element({ className: 'Period_TwinChart-title' }, [options.title]),
        Element({ className: 'Period_TwinChart-image' }, [
            Element({
                className: 'Period_TwinChart-image-items',
            }, rendered_bands.map(band => (
                options.items.map((item, i) => {

                    const time = item.time
                    const value = band.rendered_values[i]
                    const left_ratio = (time - filter.from / 1000) / ((filter.to - filter.from) / 1000)
                    const height_ratio = value / max_value * 0.5

                    return Element({
                        className: 'Period_TwinChart-image-item ' + (value > 0 ? 'positive' : 'negative'),
                        style: {
                            backgroundColor: band.band.color,
                            left: (left_ratio * 100) + '%',
                            width: 'calc(' + (width_ratio * 100) + '% + 1px)',
                            height: (Math.abs(height_ratio) * 100) + '%',
                        },
                    })

                })
            )).flat()),
            Element({ className: 'Period_TwinChart-image-lines' }, [
                ...options.steps(max_value).map((value, index) => (
                    [-1, 1].map(sign => {

                        const percent = Math.round(value / max_value * 50) + '%'

                        return Element({
                            className: 'Period_TwinChart-image-line ' +
                                (sign > 0 ? 'positive' : 'negative') +
                                (index === 0 ? ' first' : ''),
                            style: { height: percent },
                        }, [
                            Element({
                                className: 'Period_TwinChart-image-line-value',
                            }, [format(value)]),
                            Element({
                                className: 'Period_TwinChart-image-line-percent',
                            }, [percent]),
                        ])

                    })
                )).flat(),
                Element({ className: 'Period_TwinChart-image-lines-zero' }),
            ]),
            Element({ className: 'Period_TwinChart-image-border' }),
        ]),
    ])

}
;
function PeriodSelect_Days (application, number) {
    return {
        text: number + 'd',
        update (render) {

            function next () {

                const to_date = new Date(application.time_now())
                to_date.setUTCMilliseconds(0)
                to_date.setUTCSeconds(0)

                const from_date = new Date(to_date.getTime())
                from_date.setUTCDate(from_date.getUTCDate() - number)

                timeout = setTimeout(next, 1000 * 60 * 60)

                render({
                    from: from_date.getTime(),
                    to: to_date.getTime(),
                })

            }

            let timeout
            next()

            return () => {
                clearTimeout(timeout)
            }

        },
    }
}
;
function PeriodSelect_Main (application, options) {

    const right_element = Element({ className: 'PeriodSelect_Main-right' })

    const element = Element({ className: 'PeriodSelect_Main' }, [
        Element({ className: 'PeriodSelect_Main-left' }, [
            PeriodSelect_Menu(application, {
                update (filter) {
                    while (right_element.lastChild) {
                        right_element.lastChild.remove()
                    }
                    right_element.append(Element({
                        className: 'PeriodSelect_Main-right-time',
                    }, [FormatTime(filter.from)]))
                    right_element.append(Element({
                        className: 'PeriodSelect_Main-right-separator',
                    }, [' - ']))
                    right_element.append(Element({
                        className: 'PeriodSelect_Main-right-time',
                    }, [FormatTime(filter.to)]))
                    options.update(filter)
                },
            }),
        ]),
        right_element,
    ])

    application.event.scroll.listen(top => {
        if (top === 0) element.classList.remove('scroll')
        else element.classList.add('scroll')
    })

    return element

}
;
function PeriodSelect_Menu (application, options) {

    function add (item_options) {

        function deselect () {
            destroy()
            button.classList.remove('active')
        }

        function select () {
            destroy = item_options.update(options.update)
            button.classList.add('active')
            deselect_selected = deselect
            button_node.nodeValue = item_options.text
            hide()
        }

        let destroy

        const button = Element('button', {
            className: 'PeriodSelect_Menu-item',
            onclick () {
                deselect_selected()
                select()
            },
        }, [item_options.text])

        menu_element.append(button)
        items.push(select)

    }

    function hide () {
        visible = false
        button.classList.remove('active')
        menu_element.classList.add('hidden')
        removeEventListener('focus', window_focus, true)
        removeEventListener('mousedown', window_mousedown)
    }

    function window_focus (e) {
        const target = e.target
        if (target === window || target === document) return
        if (element.contains(target)) return
        hide()
    }

    function window_mousedown (e) {
        if (element.contains(e.target)) return
        hide()
    }

    let deselect_selected
    let visible = false
    const items = []

    const button_node = document.createTextNode('')

    const button = Element('button', {
        className: 'PeriodSelect_Menu-button',
        style: {
            backgroundImage: 'url(' + application.resource_url('img/dark/point-bottom.svg') + ')',
        },
        onclick () {

            if (visible) {
                hide()
                return
            }

            visible = true
            button.classList.add('active')
            menu_element.classList.remove('hidden')
            addEventListener('focus', window_focus, true)
            addEventListener('mousedown', window_mousedown)

        },
    }, [button_node])

    const menu_element = Element({
        className: 'PeriodSelect_Menu-menu hidden',
    })
    add(PeriodSelect_OneHour(application))
    add(PeriodSelect_Days(application, 1))
    add(PeriodSelect_Days(application, 7))

    const element = Element({ className: 'PeriodSelect_Menu' }, [
        button,
        menu_element,
    ])

    application.event.load.listen(items[0])

    return element

}
;
function PeriodSelect_OneHour (application) {
    return {
        text: '1h',
        update (render) {

            function next () {

                const to_date = new Date(application.time_now())
                to_date.setUTCMilliseconds(0)
                to_date.setUTCSeconds(0)

                const from_date = new Date(to_date.getTime())
                from_date.setUTCHours(from_date.getUTCHours() - 1)

                timeout = setTimeout(next, 1000 * 60)

                render({
                    from: from_date.getTime(),
                    to: to_date.getTime(),
                })

            }

            let timeout
            next()

            return () => {
                clearTimeout(timeout)
            }

        },
    }
}
;
function Post (options) {

    const request = new XMLHttpRequest
    request.open('post', options.url)
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    ;(() => {

        const headers = options.headers
        if (headers === undefined) return

        Object.keys(headers).forEach(name => {
            request.setRequestHeader(name, headers[name])
        })

    })()
    request.responseType = 'json'
    request.send(options.body || '')
    request.onerror = options.error
    request.onload = () => {

        if (request.status !== 200) {
            options.error()
            return
        }

        options.load(request.response)

    }

    return () => {
        request.abort()
    }

}
;
function Top (application, options) {

    const menu = TopMenu(application, options)

    return {
        destroy: menu.destroy,
        element: Element({
            className: 'Top',
            style: { backgroundImage: 'url(img/top.svg)' },
        }, [
            Element({ className: 'Top-title' }, ['Ton']),
            Element({ className: 'Top-right' }, [menu.element]),
        ]),
    }

}
;
function TopMenu (application, options) {

    function hide () {
        visible = false
        button.classList.remove('active')
        menu_element.classList.add('hidden')
        removeEventListener('focus', window_focus, true)
        removeEventListener('mousedown', window_mousedown)
    }

    function window_focus (e) {
        const target = e.target
        if (target === window || target === document) return
        if (element.contains(target)) return
        hide()
    }

    function window_mousedown (e) {
        if (element.contains(e.target)) return
        hide()
    }

    let visible = false
    let abort = () => {}

    const button = Element({
        className: 'TopMenu-button',
        style: {
            backgroundImage: 'url(' + application.resource_url('img/white/menu.svg') + ')',
        },
        onclick () {

            if (visible) {
                hide()
                return
            }

            visible = true
            button.classList.add('active')
            menu_element.classList.remove('hidden')
            addEventListener('focus', window_focus, true)
            addEventListener('mousedown', window_mousedown)

        },
    })

    const menu_element = Element({ className: 'TopMenu-menu hidden' }, [
        TopMenuItem(application, {
            text: 'Change username',
            click () {
                const hide = application.show_form(UsernameForm(application, {
                    done () {
                        hide()
                    },
                    logout () {
                        hide()
                        abort()
                        options.logout()
                    },
                }))
            },
        }),
        TopMenuItem(application, {
            text: 'Change password',
            click () {
                const hide = application.show_form(PasswordForm({
                    done () {
                        hide()
                    },
                    logout () {
                        hide()
                        abort()
                        options.logout()
                    },
                }))
            },
        }),
        TopMenuItem(application, {
            text: 'Log out',
            click () {
                abort()
                abort = Post({
                    url: 'api/logout/',
                    error () {},
                    load (response) {
                        if (response === true) options.logout()
                    },
                })
            },
        }),
    ])

    const element = Element({ className: 'TopMenu' }, [
        button,
        menu_element,
    ])

    return {
        element,
        destroy () {
            abort()
        },
    }

}
;
function TopMenuItem (application, options) {
    return Element('button', {
        className: 'TopMenuItem',
        onclick: options.click,
    }, [options.text])
}
;
function TwoDigitPad (n) {
    let s = n.toString()
    if (s.length === 1) s = '0' + s
    return s
}
;
function UsernameForm (application, options) {

    const username_input = Form_TextInput({
        name: 'username',
        label: 'Username',
        required: true,
        value: application.variables.session_user.username,
    })

    const current_password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'current_password',
        label: 'Current password',
        required: true,
    })

    const form = Form({
        title: 'Change username',
        action: 'username/',
        logout: options.logout,
        items: [
            username_input.element,
            current_password_input.element,
            Form_Button({ text: 'Save' }),
        ],
        submit () {

            const username = username_input.get_value()
            if (username === null) return

            const current_password = current_password_input.get_value()
            if (current_password === null) return

            return {
                username: username,
                current_password: current_password,
            }

        },
        done (response, params) {

            if (response === true) {
                application.variables.session_user.username = params.username
                options.done()
                return
            }

            if (response === 'CURRENT_PASSWORD_INVALID') {
                form.error('Invalid current password')
                return
            }

        },
    })

    return {
        destroy: form.destroy,
        element: form.element,
        focus: username_input.focus,
    }

}
;

})()