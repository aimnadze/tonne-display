<?php

class Db {

    private $tables = [];

    function __construct () {
        foreach (tables() as $name) {
            $this->tables[$name] = new Table(
                call_user_func("$name\\definition")['name']);
        }
    }

    function __get ($name) {
        if (array_key_exists($name, $this->tables)) {
            return $this->tables[$name];
        }
        throw new Exception('Table "' . json_encode($name) . '" not defined');
    }

}
