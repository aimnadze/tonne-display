<?php

function mysqli_select_single_assoc (
    $mysqli, $table, $where, $order_by = null) {

    $sql = "select * from `" . $mysqli->real_escape_string($table) . "`" .
        ' ' . mysqli_where($mysqli, $where) .
        ($order_by === null ? '' : " order by $order_by") .
        ' limit 1';

    return mysqli_single_assoc($mysqli, $sql);

}
