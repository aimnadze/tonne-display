<?php

function unit_test ($fn, $tests) {
    foreach ($tests as $test) {

        $args = $test['args'];
        $expected_result = $test['expected_result'];

        $actual_result = call_user_func_array($fn, $args);
        if ($actual_result === $expected_result) continue;

        echo json_encode($args) . "\n\n" .
            "Expected:\n" .
            json_encode($expected_result) . "\n\n" .
            "Got:\n" .
            json_encode($actual_result) . "\n\n";

    }
}
