<?php

function request_filter () {
    $from = request_uint('from');
    return [
        'from' => $from,
        'to' => max($from + 1, request_uint('to')),
    ];
}
