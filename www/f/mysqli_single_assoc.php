<?php

function mysqli_single_assoc ($mysqli, $sql) {
    $result = mysqli_safe_query($mysqli, $sql);
    $object = $result->fetch_assoc();
    $result->close();
    return $object;
}
