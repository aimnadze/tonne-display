<?php

function mysqli_in ($mysqli, $field, $values) {
    if (!$values) return '0';
    return
        '`' . $mysqli->real_escape_string($field) . '`' .
        ' in (' . join(', ', array_map(function ($value) use ($mysqli) {
            return mysqli_escape($mysqli, $value);
        }, $values)) . ')';
}
