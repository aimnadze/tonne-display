<?php

function mysqli_query_assoc ($mysqli, $sql) {
    $result = mysqli_safe_query($mysqli, $sql);
    $objects = [];
    while (true) {
        $object = $result->fetch_assoc();
        if ($object === null) break;
        $objects[] = $object;
    }
    $result->close();
    return $objects;
}
