<?php

namespace ErrorPage;

function badRequest () {

    $description = 'The page ' .
        '<em>' . htmlspecialchars($_SERVER['REQUEST_URI']) . '</em>' .
        ' cannot be accessed this way.';

    create(400, 'Bad Request', $description);

}
