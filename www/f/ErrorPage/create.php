<?php

namespace ErrorPage;

function create ($code, $text, $description) {

    $html =
        '<!DOCTYPE html>' .
        '<html style="height: 100%">' .
            '<head>' .
                "<title>$code $text</title>" .
                '<meta charset="UTF-8" />' .
                '<meta name="viewport" content="width=device-width" />' .
                '<style>' .
                    '* {' .
                        'margin: 0;' .
                        'padding: 0;' .
                        'font: normal 16px/18px Arial, sans-serif;' .
                        'box-sizing: border-box;' .
                        'word-wrap: break-word;' .
                    '}' .
                    'html, body {' .
                        'height: 100%;' .
                        'white-space: nowrap;' .
                        'text-align: center;' .
                        'background: #fff;' .
                        'color: #000;' .
                    '}' .
                    'h1 {' .
                        'font-weight: bold;' .
                        'font-size: 22px;' .
                    '}' .
                    '.layout {' .
                        'display: inline-block;' .
                        'vertical-align: middle;' .
                    '}' .
                    '.layout.aligner {' .
                        'height: 100%;' .
                    '}' .
                    '.layout.content {' .
                        'padding: 8px;' .
                        'white-space: normal;' .
                        'max-width: 100%;' .
                    '}' .
                    'em {' .
                        'border-bottom: 1px dotted;' .
                    '}' .
                '</style>' .
            '</head>' .
            '<body>' .
                '<div class="layout aligner"></div>' .
                '<div class="layout content">' .
                    "<h1>$code $text</h1>" .
                    '<br />' .
                    "<div>$description</div>" .
                '</div>' .
            '</body>' .
        '</html>';

    http_response_code($code);
    header('Content-Type: text/html; charset=UTF-8');
    die($html);

}
