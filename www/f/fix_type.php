<?php

function fix_type ($value, $type) {
    if ($type === 'bool') {
        $value = (bool)$value;
    } elseif ($type === 'int') {
        $value = (int)$value;
    } elseif ($type === 'float') {
        $value = (float)$value;
    } elseif ($type === 'string') {
        $value = is_array($value) ? '' : (string)$value;
    } elseif (is_array($type)) {
        if (!is_array($value)) $value = [];
        if (array_keys($type) === [0]) {
            $value = array_map(function ($value) use ($type) {
                return fix_type($value, $type[0]);
            }, array_values($value));
        } else {
            foreach ($type as $key => $subvalue) {
                if (!array_key_exists($key, $value)) $value[$key] = null;
                $value[$key] = fix_type($value[$key], $subvalue);
            }
        }
    } else {
        fatal('Invalid type: ' . json_encode($type));
    }
    return $value;
}
