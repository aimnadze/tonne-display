<?php

function session_ensure () {
    session_set_cookie_params(60 * 60 * 24 * 30, site_base());
    session_name('s');
    session_start();
}
