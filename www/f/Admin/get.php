<?php

namespace Admin;

function get () {
    return [
        'username' => 'admin',
        'password_hash' => '$2y$10$usHRs.mElAzV1vGfMZrxkOqqMlIGTYW0Ak./g3n8Mj6lnRlJs6nqi',
    ];
}
