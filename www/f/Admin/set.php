<?php

namespace Admin;

function set ($value) {

    $content =
        "<?php\n\n" .
        "namespace Admin;\n\n" .
        "function get () {\n" .
        '    return ' . var_export($value, true) . ";\n" .
        "}\n";

    file_put_php(__DIR__ . '/get.php', $content);

}
