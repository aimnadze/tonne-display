<?php

namespace Read;

function index ($agents, $filter) {

    $power = 0;
    $width_ratio = 60 / ($filter['to'] - $filter['from']);
    while ($width_ratio < 0.005) {
        $power++;
        $width_ratio *= 2;
    }

    $fields = [
        'loadavg_0', 'loadavg_1', 'loadavg_2',
        'totalmem', 'freemem', 'num_cpus',
        'swap_total', 'swap_used',
        'network_send', 'network_receive',
        'storage_total', 'storage_used',
        'io_read', 'io_write',
    ];

    $float_fields = [
        'loadavg_0', 'loadavg_1', 'loadavg_2',
    ];

    $sql = 'select agent_id, min(insert_time) insert_time,' .
        ' ' . join(', ', array_map(function ($field) use ($float_fields) {
            if (in_array($field, $float_fields)) {
                return "avg($field) $field";
            }
            return "round(avg($field)) $field";
        }, $fields)) .
        ' from `read`' .
        ' ' . mysqli_where(mysqli(), [
            "insert_time >= $filter[from]",
            "insert_time < $filter[to]",
            mysqli_in(mysqli(), 'agent_id', array_map(function ($agent) {
                return $agent['id'];
            }, $agents)),
        ]) .
        " group by agent_id, insert_minute_$power";

    return mysqli_query_assoc(mysqli(), $sql);

}
