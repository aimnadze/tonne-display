<?php

namespace Read;

function definition () {

    $columns = [
        'agent_id' => \Column\uint(),
        'freemem' => \Column\uint(),
        'id' => \Column\uint(true),
        'insert_time' => \Column\uint(),
        'io_read' => \Column\uint(),
        'io_read' => \Column\uint(),
        'io_write' => \Column\uint(),
        'loadavg_0' => [
            'type' => 'double unsigned',
        ],
        'loadavg_1' => [
            'type' => 'double unsigned',
        ],
        'loadavg_2' => [
            'type' => 'double unsigned',
        ],
        'network_receive' => \Column\uint(),
        'network_send' => \Column\uint(),
        'num_cpus' => \Column\uint(),
        'storage_total' => \Column\uint(),
        'storage_used' => \Column\uint(),
        'swap_total' => \Column\uint(),
        'swap_used' => \Column\uint(),
        'totalmem' => \Column\uint(),
    ];

    $indices = [
        ['agent_id'],
        ['insert_time'],
    ];

    for ($i = 0; $i < 16; $i++) {
        $columns["insert_minute_$i"] = \Column\uint();
        $indices[] = ["insert_minute_$i"];
    }

    return [
        'name' => 'read',
        'columns' => $columns,
        'indices' => $indices,
    ];

}
