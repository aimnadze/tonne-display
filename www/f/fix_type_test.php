<?php

include_once '../lib/init-cli.php';

unit_test('fix_type', [

    [
        'args' => [0, 'bool'],
        'expected_result' => false,
    ],
    [
        'args' => [0.0, 'bool'],
        'expected_result' => false,
    ],
    [
        'args' => ['', 'bool'],
        'expected_result' => false,
    ],
    [
        'args' => [false, 'bool'],
        'expected_result' => false,
    ],
    [
        'args' => [true, 'bool'],
        'expected_result' => true,
    ],

    [
        'args' => [false, 'int'],
        'expected_result' => 0,
    ],
    [
        'args' => [0.0, 'int'],
        'expected_result' => 0,
    ],
    [
        'args' => ['', 'int'],
        'expected_result' => 0,
    ],
    [
        'args' => [0, 'int'],
        'expected_result' => 0,
    ],
    [
        'args' => [1, 'int'],
        'expected_result' => 1,
    ],

    [
        'args' => [false, 'string'],
        'expected_result' => '',
    ],
    [
        'args' => [0, 'string'],
        'expected_result' => '0',
    ],
    [
        'args' => [0.0, 'string'],
        'expected_result' => '0',
    ],
    [
        'args' => ['', 'string'],
        'expected_result' => '',
    ],
    [
        'args' => ['a', 'string'],
        'expected_result' => 'a',
    ],

    [
        'args' => [false, []],
        'expected_result' => [],
    ],
    [
        'args' => [0, []],
        'expected_result' => [],
    ],
    [
        'args' => [0.0, []],
        'expected_result' => [],
    ],
    [
        'args' => ['', []],
        'expected_result' => [],
    ],
    [
        'args' => [[], []],
        'expected_result' => [],
    ],
    [
        'args' => [[false], []],
        'expected_result' => [false],
    ],

    [
        'args' => [[], [
            'field1' => 'bool',
            'field2' => 'int',
            'field3' => 'float',
            'field4' => 'string',
            'field5' => [],
        ]],
        'expected_result' => [
            'field1' => false,
            'field2' => 0,
            'field3' => 0.0,
            'field4' => '',
            'field5' => [],
        ],
    ],
    [
        'args' => [[
            'field1' => true,
            'field2' => 1,
            'field3' => 1.1,
            'field4' => 'a',
            'field5' => [false],
        ], [
            'field1' => 'bool',
            'field2' => 'int',
            'field3' => 'float',
            'field4' => 'string',
            'field5' => [],
        ]],
        'expected_result' => [
            'field1' => true,
            'field2' => 1,
            'field3' => 1.1,
            'field4' => 'a',
            'field5' => [false],
        ],
    ],

    [
        'args' => [[
            'field1' => [false, 0, 0.0, '', []],
            'field2' => [false, 0, 0.0, '', []],
            'field3' => [false, 0, 0.0, '', []],
            'field4' => [false, 0, 0.0, '', []],
            'field5' => [false, 0, 0.0, '', []],
        ], [
            'field1' => ['bool'],
            'field2' => ['int'],
            'field3' => ['float'],
            'field4' => ['string'],
            'field5' => [[]],
        ]],
        'expected_result' => [
            'field1' => [false, false, false, false, false],
            'field2' => [0, 0, 0, 0, 0],
            'field3' => [0.0, 0.0, 0.0, 0.0, 0.0],
            'field4' => ['', '0', '0', '', ''],
            'field5' => [[], [], [], [], []],
        ],
    ],
    [
        'args' => [[
            'field1' => [true, 1, 1.1, 'a', [false]],
            'field2' => [true, 1, 1.1, 'a', [false]],
            'field3' => [true, 1, 1.1, 'a', [false]],
            'field4' => [true, 1, 1.1, 'a', [false]],
            'field5' => [true, 1, 1.1, 'a', [false]],
        ], [
            'field1' => ['bool'],
            'field2' => ['int'],
            'field3' => ['float'],
            'field4' => ['string'],
            'field5' => [[]],
        ]],
        'expected_result' => [
            'field1' => [true, true, true, true, true],
            'field2' => [1, 1, 1, 0, 1],
            'field3' => [1.0, 1.0, 1.1, 0.0, 1.0],
            'field4' => ['1', '1', '1.1', 'a', ''],
            'field5' => [[], [], [], [], [false]],
        ],
    ],

]);
