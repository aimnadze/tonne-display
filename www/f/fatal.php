<?php

function fatal ($error) {

    if (debug_mode()) {
        trigger_error($error);
        exit(1);
    }

    if (php_sapi_name() === 'cli') {
        echo "ERROR: $error\n";
        exit(1);
    }

    $reference = bin2hex(openssl_random_pseudo_bytes(20));
    error_log("$reference: $error");

    ErrorPage\internalServerError($reference);

}
