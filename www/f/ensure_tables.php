<?php

function ensure_tables () {
    return join(array_map(function ($table) {
        return Table\ensure(mysqli(), call_user_func("$table\\definition"));
    }, tables()));
}
