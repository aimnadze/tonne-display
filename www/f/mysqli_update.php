<?php

function mysqli_update ($mysqli, $table, $set, $where) {

    $assignments = [];
    foreach ($set as $key => $value) {
        $assignments[] = "`" . $mysqli->real_escape_string($key) . "`" .
            ' = ' . mysqli_escape($mysqli, $value);
    }

    $sql = "update `" . $mysqli->real_escape_string($table) . "`" .
        ' set ' . join(', ', $assignments) .
        ' ' . mysqli_where($mysqli, $where);

    mysqli_safe_query($mysqli, $sql);

    return $mysqli->affected_rows;

}
