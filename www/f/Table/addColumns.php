<?php

namespace Table;

function addColumns ($mysqli, $table_name, $columns) {

    $table_name = $mysqli->real_escape_string($table_name);

    $output = '';
    foreach ($columns as $name => $column) {

        $name = $mysqli->real_escape_string($name);

        $sql = "alter table `$table_name`" .
            " add `$name` " . columnDefinition($column);

        $output .= "SQL: $sql\n";

        mysqli_safe_query($mysqli, $sql);

    }

    return $output;

}
