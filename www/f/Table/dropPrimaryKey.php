<?php

namespace Table;

function dropPrimaryKey ($mysqli, $table_name) {

    $sql = "alter table `" . $mysqli->real_escape_string($table_name) . "`" .
        ' drop primary key';

    mysqli_safe_query($mysqli, $sql);

    return "SQL: $sql\n";

}
