<?php

namespace Table;

function editColumn ($mysqli, $table_name, $column_name, $column) {

    $table_name = $mysqli->real_escape_string($table_name);
    $column_name = $mysqli->real_escape_string($column_name);

    $sql = "alter table `$table_name` change `$column_name`" .
        " `$column_name` " . columnDefinition($column);

    mysqli_safe_query($mysqli, $sql);

    return "SQL: $sql\n";

}
