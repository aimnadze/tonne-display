<?php

namespace Table;

function create ($mysqli, $table_name, $columns) {

    $table_name = $mysqli->real_escape_string($table_name);

    $sql = "create table `$table_name` (";
    $first = true;
    foreach ($columns as $name => $column) {

        if ($first) $first = false;
        else $sql .= ', ';

        $name = $mysqli->real_escape_string($name);

        $sql .= "`$name` " . columnDefinition($column);

    }
    $sql .= ') collate=utf8mb4_general_ci';

    mysqli_safe_query($mysqli, $sql);

    return "SQL: $sql\n";

}
