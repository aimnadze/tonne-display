<?php

function mysqli_escape ($mysqli, $value) {
    if (is_array($value)) return $value[0];
    if ($value === null) return 'null';
    if (is_bool($value)) return $value ? '1' : '0';
    if (is_int($value) || is_float($value)) return $value;
    return "'" . $mysqli->real_escape_string($value) . "'";
}
