<?php

include_once 'lib/init-web.php';

if (debug_mode()) {

    $css_links = join(array_map(function ($file) {
        return "<link rel=\"stylesheet\" href=\"$file\" />";
    }, json_decode(file_get_contents('json/css-files.json'))));

    $js_scripts = join(array_map(function ($file) {
        return "<script src=\"$file\">" .
            '</script>';
    }, json_decode(file_get_contents('json/js-files.json'))));

} else {

    $css_links = '<link rel="stylesheet"' .
        ' href="compressed.css?' . revisions()['compressed.css'] . '" />';

    $js_scripts =
        '<script src="compressed.js?' . revisions()['compressed.js'] . '">' .
        '</script>';

}

$html =
    '<!DOCTYPE html>' .
    '<html>' .
        '<head>' .
            '<title>Ton</title>' .
            '<meta charset="UTF-8" />' .
            '<meta name="viewport" content="width=device-width" />' .
            '<link rel="icon" href="img/icon/16.png" sizes="16x16" />' .
            '<link rel="icon" href="img/icon/32.png" sizes="32x32" />' .
            '<link rel="icon" href="img/icon/128.png" sizes="128x128" />' .
            $css_links .
        '</head>' .
        '<body>' .
            $js_scripts .
            '<script>' .
                'Main(' . json_encode([
                    'time' => floor(microtime(true) * 1000),
                    'site_base' => site_base(),
                    'session_user' => array_key_exists('user', $_SESSION) ? [
                        'username' => Admin\get()['username'],
                    ] : null,
                ]) . ')' .
            '</script>' .
        '</body>' .
    '</html>';

header('Content-Type: text/html; charset=UTF-8');
echo $html;
