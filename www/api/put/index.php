<?php

include_once '../../lib/init-web.php';

$request = fix_type(json_decode(file_get_contents('php://input'), true), [
    'username' => 'string',
    'password' => 'string',
    'minute' => [
        'time' => 'int',
        'os' => [
            'loadavg' => [
                'float',
                'float',
                'float',
            ],
            'totalmem' => 'int',
            'freemem' => 'int',
            'num_cpus' => 'int',
        ],
        'swap' => [
            'total' => 'int',
            'used' => 'int',
        ],
        'network' => [
            'send' => 'int',
            'receive' => 'int',
        ],
        'storage' => [
            'total' => 'int',
            'used' => 'int',
        ],
        'io' => [
            'read' => 'int',
            'write' => 'int',
        ],
    ],
]);

$agent = db()->Agent->single_assoc([
    'name' => $request['username'],
]);
if ($agent === null || !password_verify($request['password'], $agent['password_hash'])) {
    respond('INVALID_LOGIN');
}

$minute = $request['minute'];
$os = $minute['os'];
$swap = $minute['swap'];
$storage = $minute['storage'];
$network = $minute['network'];
$io = $minute['io'];

$values = [
    'agent_id' => $agent['id'],
    'loadavg_0' => $os['loadavg'][0],
    'loadavg_1' => $os['loadavg'][1],
    'loadavg_2' => $os['loadavg'][2],
    'totalmem' => $os['totalmem'],
    'freemem' => $os['freemem'],
    'num_cpus' => $os['num_cpus'],
    'swap_total' => $swap['total'],
    'swap_used' => $swap['used'],
    'network_send' => $network['send'],
    'network_receive' => $network['receive'],
    'storage_total' => $storage['total'],
    'storage_used' => $storage['used'],
    'io_read' => $io['read'],
    'io_write' => $io['write'],
    'insert_time' => $minute['time'],
];

for ($i = 0; $i < 16; $i++) {
    $values["insert_minute_$i"] = floor($minute['time'] / 60 / pow(2, $i));
}

db()->Read->insert($values);

respond(true);
