<?php

include_once '../lib/init-valid.php';

session_commit();

$agents = db()->Agent->query_assoc([], 'name');

respond([
    'agents' => array_map(function ($agent) {
        return [
            'id' => (int)$agent['id'],
            'name' => $agent['name'],
        ];
    }, $agents),
    'reads' => array_map(function ($read) {
        return [
            'agent_id' => (int)$read['agent_id'],
            'time' => (int)$read['insert_time'],
            'os' => [
                'loadavg' => [
                    (float)$read['loadavg_0'],
                    (float)$read['loadavg_1'],
                    (float)$read['loadavg_2'],
                ],
                'totalmem' => (int)$read['totalmem'],
                'freemem' => (int)$read['freemem'],
                'num_cpus' => (int)$read['num_cpus'],
            ],
            'swap' => [
                'total' => (int)$read['swap_total'],
                'used' => (int)$read['swap_used'],
            ],
            'network' => [
                'send' => (int)$read['network_send'],
                'receive' => (int)$read['network_receive'],
            ],
            'storage' => [
                'total' => (int)$read['storage_total'],
                'used' => (int)$read['storage_used'],
            ],
            'io' => [
                'read' => (int)$read['io_read'],
                'write' => (int)$read['io_write'],
            ],
        ];
    }, Read\index($agents, request_filter())),
]);
