<?php

date_default_timezone_set('UTC');

include_once __DIR__ . '/../f/db.php';
include_once __DIR__ . '/../f/debug_mode.php';
include_once __DIR__ . '/../f/fatal.php';
include_once __DIR__ . '/../f/file_put_php.php';
include_once __DIR__ . '/../f/fix_type.php';
include_once __DIR__ . '/../f/mysql_config.php';
include_once __DIR__ . '/../f/mysqli.php';
include_once __DIR__ . '/../f/mysqli_delete.php';
include_once __DIR__ . '/../f/mysqli_escape.php';
include_once __DIR__ . '/../f/mysqli_in.php';
include_once __DIR__ . '/../f/mysqli_insert.php';
include_once __DIR__ . '/../f/mysqli_query_assoc.php';
include_once __DIR__ . '/../f/mysqli_safe_query.php';
include_once __DIR__ . '/../f/mysqli_select_query_assoc.php';
include_once __DIR__ . '/../f/mysqli_select_single_assoc.php';
include_once __DIR__ . '/../f/mysqli_single_assoc.php';
include_once __DIR__ . '/../f/mysqli_update.php';
include_once __DIR__ . '/../f/mysqli_where.php';
include_once __DIR__ . '/../f/mysqli_where_conditions.php';
include_once __DIR__ . '/../f/register_autoload.php';
include_once __DIR__ . '/../f/request_string.php';
include_once __DIR__ . '/../f/request_uint.php';
include_once __DIR__ . '/../f/revisions.php';
include_once __DIR__ . '/../f/site_base.php';
include_once __DIR__ . '/../f/str_collapse_spaces.php';
include_once __DIR__ . '/../f/tables.php';
include_once __DIR__ . '/../f/Admin/get.php';
include_once __DIR__ . '/../f/Admin/set.php';
include_once __DIR__ . '/../f/Column/string.php';
include_once __DIR__ . '/../f/Column/uint.php';
include_once __DIR__ . '/../f/ErrorPage/badRequest.php';
include_once __DIR__ . '/../f/ErrorPage/create.php';
include_once __DIR__ . '/../f/ErrorPage/forbidden.php';
include_once __DIR__ . '/../f/ErrorPage/internalServerError.php';
include_once __DIR__ . '/../f/ErrorPage/notFound.php';
include_once __DIR__ . '/../f/Password/column.php';
include_once __DIR__ . '/../f/Read/index.php';

foreach (tables() as $table) {
    include_once __DIR__ . "/../f/$table/definition.php";
}

register_autoload();
