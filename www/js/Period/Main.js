function Period_Main (options) {

    const filter = options.filter

    return Post({
        url: 'api/fetch/',
        body: BuildQuery({
            from: Math.floor(filter.from / 1000),
            to: Math.floor(filter.to / 1000),
        }),
        error () {},
        load (response) {

            if (response === 'LOG_IN') {
                options.logout()
                return
            }

            const agent_map_items = {}
            response.agents.forEach(agent => {
                agent_map_items[agent.id] = []
            })
            response.reads.forEach(item => {
                const items = agent_map_items[item.agent_id]
                if (items === undefined) return
                items.push(item)
            })

            options.done(Element({
                className: 'Period_Main',
            }, response.agents.map(agent => (
                Period_Item(filter, agent, agent_map_items[agent.id])
            ))))

        },
    })

}
