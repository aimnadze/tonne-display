function Period_Item (filter, agent, items) {

    function item (element, className) {
        return Element({
            className: 'Period_Item-item' + (className === undefined ? '' : ' ' + className),
        }, [element])
    }

    return Element({ className: 'Period_Item' }, [
        Element('h2', { className: 'Period_Item-title' }, [agent.name]),
        Element({ className: 'Period_Item-content' }, [
            item(Period_Processor(filter, items), 'processor'),
            item(Period_Memory(filter, items)),
            item(Period_Storage(filter, items)),
            item(Period_Network(filter, items), 'network'),
            item(Period_IO(filter, items), 'io'),
        ]),
    ])

}
