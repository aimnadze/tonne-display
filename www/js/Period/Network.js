function Period_Network (filter, items) {
    return Period_TwinChart({
        title: 'Network In / Out',
        filter: filter,
        items: items,
        ceil: GraphCeilBytes,
        steps: GraphStepsBytes,
        bands: [{
            title: '15m',
            color: 'hsl(140, 50%, 55%)',
            render (item) {
                return item.network.receive
            },
        }, {
            title: '1m',
            color: 'hsl(320, 50%, 65%)',
            render (item) {
                return -item.network.send
            },
        }],
        format (max_value) {

            function apply (_unit) {
                max_value /= 1024
                divider *= 1024
                unit = _unit
            }

            let unit = 'B/s'
            let divider = 1
            if (max_value >= 1024) apply('KB/s')
            if (max_value >= 1024) apply('MB/s')
            if (max_value >= 1024) apply('GB/s')

            return value => (
                (value / divider).toFixed(1).replace(/\.?0+$/g, '') + ' ' + unit
            )

        },
    })
}
