function Period_Processor (filter, items) {
    return Period_Chart({
        title: 'Processor',
        filter: filter,
        items: items,
        ceil: GraphCeil,
        steps: GraphSteps,
        bands: [{
            color: 'hsl(180, 50%, 85%)',
            render (item) {
                return item.os.num_cpus
            },
        }, {
            color: 'hsl(140, 50%, 55%)',
            render (item) {
                return item.os.loadavg[0]
            },
        }],
        format () {
            return value => value
        },
    })
}
