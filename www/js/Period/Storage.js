function Period_Storage (filter, items) {
    return Period_Chart({
        title: 'Storage',
        filter: filter,
        items: items,
        ceil: GraphCeilBytes,
        steps: GraphStepsBytes,
        bands: [{
            color: 'hsl(180, 50%, 85%)',
            render (item) {
                return item.storage.total
            },
        }, {
            color: 'hsl(140, 50%, 55%)',
            render (item) {
                return item.storage.used
            },
        }],
        format (max_value) {

            function apply (_unit) {
                max_value /= 1024
                divider *= 1024
                unit = _unit
            }

            let unit = 'KB'
            let divider = 1
            if (max_value >= 1024) apply('MB')
            if (max_value >= 1024) apply('GB')
            if (max_value >= 1024) apply('TB')

            return value => (
                (value / divider).toFixed(1).replace(/\.?0+$/g, '') + ' ' + unit
            )

        },
    })
}
