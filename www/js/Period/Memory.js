function Period_Memory (filter, items) {
    return Period_TwinChart({
        title: 'Memory / Swap',
        filter: filter,
        items: items,
        ceil: GraphCeilBytes,
        steps: GraphStepsBytes,
        bands: [{
            color: 'hsl(180, 50%, 85%)',
            render (item) {
                return item.os.totalmem
            },
        }, {
            color: 'hsl(140, 50%, 55%)',
            render (item) {
                return item.os.totalmem - item.os.freemem
            },
        }, {
            title: '1m',
            color: 'hsl(320, 50%, 85%)',
            render (item) {
                return -item.swap.total * 1024
            },
        }, {
            title: '15m',
            color: 'hsl(280, 50%, 65%)',
            render (item) {
                return -item.swap.used * 1024
            },
        }],
        format (max_value) {

            function apply (_unit) {
                max_value /= 1024
                divider *= 1024
                unit = _unit
            }

            let unit = 'B'
            let divider = 1
            if (max_value >= 1024) apply('KB')
            if (max_value >= 1024) apply('MB')
            if (max_value >= 1024) apply('GB')
            if (max_value >= 1024) apply('TB')

            return value => (
                (value / divider).toFixed(1).replace(/\.?0+$/g, '') + ' ' + unit
            )

        },
    })
}
