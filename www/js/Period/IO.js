function Period_IO (filter, items) {
    return Period_TwinChart({
        title: 'Disk Read / Write',
        filter: filter,
        items: items,
        ceil: GraphCeilBytes,
        steps: GraphStepsBytes,
        bands: [{
            color: 'hsl(140, 50%, 55%)',
            render (item) {
                return item.io.read
            },
        }, {
            color: 'hsl(320, 50%, 65%)',
            render (item) {
                return -item.io.write
            },
        }],
        format (max_value) {

            function apply (_unit) {
                max_value /= 1024
                divider *= 1024
                unit = _unit
            }

            let unit = 'B/s'
            let divider = 1
            if (max_value >= 1024) apply('KB/s')
            if (max_value >= 1024) apply('MB/s')

            return value => (
                (value / divider).toFixed(1).replace(/\.?0+$/g, '') + ' ' + unit
            )

        },
    })
}
