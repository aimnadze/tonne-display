function Period_Chart (options) {

    const filter = options.filter

    const width_ratio = (() => {
        let width_ratio = (1000 * 60) / (filter.to - filter.from)
        while (width_ratio < 0.005) width_ratio *= 2
        return width_ratio
    })()

    const rendered_bands = options.bands.map(band => ({
        band: band,
        rendered_values: options.items.map(band.render),
    }))

    const max_value = (() => {
        let max_value = 1
        rendered_bands.forEach(band => {
            band.rendered_values.forEach(value => {
                max_value = Math.max(max_value, value)
            })
        })
        return options.ceil(max_value)
    })()

    const format = options.format(max_value)

    return Element({ className: 'Period_Chart' }, [
        Element({ className: 'Period_Chart-title' }, [options.title]),
        Element({ className: 'Period_Chart-image' }, [
            Element({
                className: 'Period_Chart-image-items',
            }, rendered_bands.map(band => (
                options.items.map((item, i) => {

                    const time = item.time
                    const value = band.rendered_values[i]
                    const left_ratio = (time - filter.from / 1000) / ((filter.to - filter.from) / 1000)
                    const height_ratio = value / max_value

                    return Element({
                        className: 'Period_Chart-image-item',
                        style: {
                            backgroundColor: band.band.color,
                            left: (left_ratio * 100) + '%',
                            width: 'calc(' + (width_ratio * 100) + '% + 1px)',
                            height: (height_ratio * 100) + '%',
                        },
                    })

                })
            )).flat()),
            Element({ className: 'Period_Chart-image-lines' }, [
                ...options.steps(max_value).map((value, index) => {

                    const percent = Math.round(value / max_value * 100) + '%'

                    return Element({
                        className: 'Period_Chart-image-line' + (index === 0 ? ' first' : ''),
                        style: { height: percent } ,
                    }, [
                        Element({
                            className: 'Period_Chart-image-line-value',
                        }, [format(value)]),
                        Element({
                            className: 'Period_Chart-image-line-percent',
                        }, [percent]),
                    ])

                }),
                Element({ className: 'Period_Chart-image-lines-zero' }),
            ]),
            Element({ className: 'Period_Chart-image-border' }),
        ]),
    ])

}
