function Period_TwinChart (options) {

    const filter = options.filter

    const width_ratio = (() => {
        let width_ratio = (1000 * 60) / (filter.to - filter.from)
        while (width_ratio < 0.005) width_ratio *= 2
        return width_ratio
    })()

    const rendered_bands = options.bands.map(band => ({
        band: band,
        rendered_values: options.items.map(band.render),
    }))

    const max_value = (() => {
        let max_value = 1
        rendered_bands.forEach(band => {
            band.rendered_values.forEach(value => {
                max_value = Math.max(max_value, Math.abs(value))
            })
        })
        return options.ceil(max_value)
    })()

    const format = options.format(max_value)

    return Element({ className: 'Period_TwinChart' }, [
        Element({ className: 'Period_TwinChart-title' }, [options.title]),
        Element({ className: 'Period_TwinChart-image' }, [
            Element({
                className: 'Period_TwinChart-image-items',
            }, rendered_bands.map(band => (
                options.items.map((item, i) => {

                    const time = item.time
                    const value = band.rendered_values[i]
                    const left_ratio = (time - filter.from / 1000) / ((filter.to - filter.from) / 1000)
                    const height_ratio = value / max_value * 0.5

                    return Element({
                        className: 'Period_TwinChart-image-item ' + (value > 0 ? 'positive' : 'negative'),
                        style: {
                            backgroundColor: band.band.color,
                            left: (left_ratio * 100) + '%',
                            width: 'calc(' + (width_ratio * 100) + '% + 1px)',
                            height: (Math.abs(height_ratio) * 100) + '%',
                        },
                    })

                })
            )).flat()),
            Element({ className: 'Period_TwinChart-image-lines' }, [
                ...options.steps(max_value).map((value, index) => (
                    [-1, 1].map(sign => {

                        const percent = Math.round(value / max_value * 50) + '%'

                        return Element({
                            className: 'Period_TwinChart-image-line ' +
                                (sign > 0 ? 'positive' : 'negative') +
                                (index === 0 ? ' first' : ''),
                            style: { height: percent },
                        }, [
                            Element({
                                className: 'Period_TwinChart-image-line-value',
                            }, [format(value)]),
                            Element({
                                className: 'Period_TwinChart-image-line-percent',
                            }, [percent]),
                        ])

                    })
                )).flat(),
                Element({ className: 'Period_TwinChart-image-lines-zero' }),
            ]),
            Element({ className: 'Period_TwinChart-image-border' }),
        ]),
    ])

}
