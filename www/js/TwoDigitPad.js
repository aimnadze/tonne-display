function TwoDigitPad (n) {
    let s = n.toString()
    if (s.length === 1) s = '0' + s
    return s
}
