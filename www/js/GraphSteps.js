function GraphSteps (n) {

    let divider = 1
    while (true) {
        if (n <= 5) break
        n /= 10
        divider *= 10
    }

    const steps = (() => {
        if (n <= 1) return [1, 0.8, 0.6, 0.4, 0.2]
        else if (n <= 2) return [2, 1.5, 1, 0.5]
        else if (n <= 4) return [4, 3, 2, 1]
        else return [5, 4, 3, 2, 1]
    })()

    for (let i = 0; i < steps.length; i++) {
        steps[i] *= divider
    }

    return steps

}
