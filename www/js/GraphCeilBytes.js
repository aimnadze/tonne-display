function GraphCeilBytes (n) {
    return Math.pow(2, Math.ceil(Math.log(n) / Math.log(2)))
}
