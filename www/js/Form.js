function Form (options) {

    let abort = () => {}

    let error_element = null

    const element = Element('form', {
        onsubmit (e) {

            abort()
            e.preventDefault()

            const params = options.submit()
            if (params === undefined) return

            abort = Post({
                url: 'api/' + options.action,
                body: BuildQuery(params),
                error () {},
                load (response) {

                    if (response === 'LOG_IN') {
                        options.logout()
                        return
                    }

                    options.done(response, params)

                },
            })

        },
    }, [
        Page_H1({ text: options.title }),
        ...options.items,
    ])

    return {
        element,
        destroy () {
            abort()
        },
        error (text) {
            error_element = Form_Error({ text: text })
            element.insertBefore(error_element, element.childNodes[1])
            abort = () => {
                element.removeChild(error_element)
                abort = () => {}
            }
        },
    }

}
