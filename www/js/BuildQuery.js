function BuildQuery (params) {

    function scan (params, prefix) {
        for (const i in params) {
            const value = (() => {
                const value = params[i]
                if (typeof value === 'boolean') return value ? '1' : ''
                return value
            })()
            if (typeof value === 'object') {
                scan(value, prefix + i + '_')
                continue
            }
            if (value === null) continue
            flat[prefix + i] = value
        }
    }

    const flat = {}
    scan(params, '', '')

    const array = []
    for (const i in flat) {
        array.push(i + '=' + encodeURIComponent(flat[i]))
    }
    return array.join('&')

}
