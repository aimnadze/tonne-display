function Event (options) {

    function listen (listener) {
        if (typeof listener !== 'function') throw new Error
        if (!options.unlimited && listeners.length === 16) {
            throw new Error('Too many listeners')
        }
        listeners.push(listener)
    }

    function unlisten (listener) {
        const index = listeners.indexOf(listener)
        if (index === -1) throw new Error
        listeners.splice(index, 1)
    }

    if (options === undefined) options = {}

    const listeners = []

    return {
        listen, unlisten,
        emit (...args) {
            listeners.slice(0).forEach(listener => {
                listener(...args)
            })
        },
        reset () {
            listeners.splice(0)
        },
    }

}
