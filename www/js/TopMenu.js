function TopMenu (application, options) {

    function hide () {
        visible = false
        button.classList.remove('active')
        menu_element.classList.add('hidden')
        removeEventListener('focus', window_focus, true)
        removeEventListener('mousedown', window_mousedown)
    }

    function window_focus (e) {
        const target = e.target
        if (target === window || target === document) return
        if (element.contains(target)) return
        hide()
    }

    function window_mousedown (e) {
        if (element.contains(e.target)) return
        hide()
    }

    let visible = false
    let abort = () => {}

    const button = Element({
        className: 'TopMenu-button',
        style: {
            backgroundImage: 'url(' + application.resource_url('img/white/menu.svg') + ')',
        },
        onclick () {

            if (visible) {
                hide()
                return
            }

            visible = true
            button.classList.add('active')
            menu_element.classList.remove('hidden')
            addEventListener('focus', window_focus, true)
            addEventListener('mousedown', window_mousedown)

        },
    })

    const menu_element = Element({ className: 'TopMenu-menu hidden' }, [
        TopMenuItem(application, {
            text: 'Change username',
            click () {
                const hide = application.show_form(UsernameForm(application, {
                    done () {
                        hide()
                    },
                    logout () {
                        hide()
                        abort()
                        options.logout()
                    },
                }))
            },
        }),
        TopMenuItem(application, {
            text: 'Change password',
            click () {
                const hide = application.show_form(PasswordForm({
                    done () {
                        hide()
                    },
                    logout () {
                        hide()
                        abort()
                        options.logout()
                    },
                }))
            },
        }),
        TopMenuItem(application, {
            text: 'Log out',
            click () {
                abort()
                abort = Post({
                    url: 'api/logout/',
                    error () {},
                    load (response) {
                        if (response === true) options.logout()
                    },
                })
            },
        }),
    ])

    const element = Element({ className: 'TopMenu' }, [
        button,
        menu_element,
    ])

    return {
        element,
        destroy () {
            abort()
        },
    }

}
