function Post (options) {

    const request = new XMLHttpRequest
    request.open('post', options.url)
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    ;(() => {

        const headers = options.headers
        if (headers === undefined) return

        Object.keys(headers).forEach(name => {
            request.setRequestHeader(name, headers[name])
        })

    })()
    request.responseType = 'json'
    request.send(options.body || '')
    request.onerror = options.error
    request.onload = () => {

        if (request.status !== 200) {
            options.error()
            return
        }

        options.load(request.response)

    }

    return () => {
        request.abort()
    }

}
