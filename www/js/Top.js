function Top (application, options) {

    const menu = TopMenu(application, options)

    return {
        destroy: menu.destroy,
        element: Element({
            className: 'Top',
            style: { backgroundImage: 'url(img/top.svg)' },
        }, [
            Element({ className: 'Top-title' }, ['Ton']),
            Element({ className: 'Top-right' }, [menu.element]),
        ]),
    }

}
