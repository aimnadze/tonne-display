function GraphStepsBytes (n) {
    const precision = Math.pow(10, 5)
    const power = Math.round(Math.log(n) / Math.log(2) * precision) / precision
    const ceil = Math.pow(2, Math.ceil(power))
    return [ceil, ceil * 3 / 4, ceil / 2, ceil / 4]
}
