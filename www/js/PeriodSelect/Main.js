function PeriodSelect_Main (application, options) {

    const right_element = Element({ className: 'PeriodSelect_Main-right' })

    const element = Element({ className: 'PeriodSelect_Main' }, [
        Element({ className: 'PeriodSelect_Main-left' }, [
            PeriodSelect_Menu(application, {
                update (filter) {
                    while (right_element.lastChild) {
                        right_element.lastChild.remove()
                    }
                    right_element.append(Element({
                        className: 'PeriodSelect_Main-right-time',
                    }, [FormatTime(filter.from)]))
                    right_element.append(Element({
                        className: 'PeriodSelect_Main-right-separator',
                    }, [' - ']))
                    right_element.append(Element({
                        className: 'PeriodSelect_Main-right-time',
                    }, [FormatTime(filter.to)]))
                    options.update(filter)
                },
            }),
        ]),
        right_element,
    ])

    application.event.scroll.listen(top => {
        if (top === 0) element.classList.remove('scroll')
        else element.classList.add('scroll')
    })

    return element

}
