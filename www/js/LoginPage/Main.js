function LoginPage_Main (application) {
    return Element({
        className: 'LoginPage_Main',
        style: { backgroundImage: 'url(img/login.svg)' },
    }, [
        Element({ className: 'LoginPage_Main-content' }, [
            LoginPage_Form(application),
        ]),
    ])
}
