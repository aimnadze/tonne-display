window.Main = variables => {

    function load (page) {
        current_page = page(application)
        element.append(current_page)
        event.load.emit()
    }

    let current_page
    const time_difference = Date.now() - variables.time

    const event = {
        load: Event(),
        scroll: Event(),
    }

    const application = {
        event, variables,
        load (page) {
            current_page.remove()
            event.load.reset()
            event.scroll.reset()
            load(page)
        },
        resource_url (path) {
            return variables.site_base + path
        },
        show_form (form) {

            function hide () {
                element.removeChild(dialog)
            }

            const dialog = Dialog({
                content: [form.element],
                close () {
                    hide()
                    form.destroy()
                },
            })
            element.append(dialog)
            form.focus()
            return hide

        },
        time_now () {
            return Date.now() - time_difference
        },
    }

    const element = Element({
        className: 'Main',
        onscroll () {
            event.scroll.emit(element.scrollTop)
        },
    })

    document.body.append(element)
    load(variables.session_user ? HomePage : LoginPage_Main)

}
