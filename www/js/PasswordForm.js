function PasswordForm (options) {

    const current_password_input = Form_TextInput({
        type: 'password',
        name: 'current_password',
        label: 'Current password',
        required: true,
    })

    const new_password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'new_password',
        label: 'New password',
        required: true,
    })

    const repeat_new_password_input = Form_TextInput({
        next: true,
        type: 'password',
        name: 'repeat_new_password',
        label: 'Repeat new password',
        required: true,
    })

    const form = Form({
        title: 'Change password',
        action: 'password/',
        logout: options.logout,
        items: [
            current_password_input.element,
            new_password_input.element,
            repeat_new_password_input.element,
            Form_Button({ text: 'Save' }),
        ],
        submit () {

            const current_password = current_password_input.get_value()
            if (current_password === null) return

            const new_password = new_password_input.get_value()
            if (new_password === null) return

            const repeat_new_password = repeat_new_password_input.get_value()
            if (repeat_new_password === null) return

            if (new_password !== repeat_new_password) {
                form.error('New passwords doesn\'t match')
                return
            }

            return {
                current_password: current_password,
                new_password: new_password,
            }

        },
        done (response) {

            if (response === true) {
                options.done()
                return
            }

            if (response === 'CURRENT_PASSWORD_INVALID') {
                form.error('Invalid current password')
                return
            }

        },
    })

    return {
        destroy: form.destroy,
        element: form.element,
        focus: current_password_input.focus,
    }

}
