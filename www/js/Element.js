function Element (...args) {

    const name = typeof args[0] === 'string' ? args.shift() : 'div'
    const properties = args[0] instanceof Array ? {} : args.shift()
    const content = args[0] instanceof Array ? args.shift() : []

    const element = document.createElement(name)
    for (const i in properties) {
        if (i === 'style') {
            Object.assign(element.style, properties[i])
            continue
        }
        element[i] = properties[i]
    }
    element.append(...content)
    return element

}
