function GraphCeil (n) {

    const steps = [1, 2, 4, 5]
    let multiplier = 1

    while (true) {
        for (let i = 0; i < steps.length; i++) {
            const snap = steps[i] * multiplier
            if (n <= snap) return snap
        }
        multiplier *= 10
    }

}
