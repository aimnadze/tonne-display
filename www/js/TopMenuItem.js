function TopMenuItem (application, options) {
    return Element('button', {
        className: 'TopMenuItem',
        onclick: options.click,
    }, [options.text])
}
