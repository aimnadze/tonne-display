function HomePage (application) {

    function logout () {
        application.load(LoginPage_Main)
    }

    let period = null

    let abort = () => {}

    const top = Top(application, {
        logout () {
            abort()
            logout()
        },
    })

    const element = Element([
        top.element,
        PeriodSelect_Main(application, {
            logout () {
                top.destroy()
                logout()
            },
            update (filter) {
                abort()
                abort = Period_Main({
                    filter, logout,
                    done (new_period) {
                        const scrollTop = element.scrollTop
                        if (period !== null) period.remove()
                        period = new_period
                        element.append(period)
                        element.scrollTop = scrollTop
                    },
                })
            },
        }),
    ])

    return element

}
