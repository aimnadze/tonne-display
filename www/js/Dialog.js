function Dialog (options) {
    const element = Element({
        className: 'Dialog',
        onclick (e) {
            if (e.target === element) options.close()
        },
    }, [
        Element({ className: 'Dialog-content' }, options.content),
    ])
    return element
}
