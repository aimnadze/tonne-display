<?php

chdir(__DIR__);
include_once '../lib/init-cli.php';

$content =
    "# auto-generated\n\n" .
    "Options -Indexes\n\n" .
    "<FilesMatch \"\.(css|js|png|svg|ttf)$\">\n" .
    "    Header set Cache-Control \"public, max-age=31536000\"\n" .
    "</FilesMatch>\n\n" .
    "<FilesMatch \"\.(css|js|svg)$\">\n" .
    "    SetOutputFilter DEFLATE\n" .
    "</FilesMatch>\n\n" .
    'ErrorDocument 403 ' . site_base() . "403.php\n" .
    'ErrorDocument 404 ' . site_base() . "404.php\n\n" .
    "Header always edit Set-Cookie (.*) \"$1; SameSite=Lax\"\n";

file_put_contents('../.htaccess', $content);

echo "Done\n";
