<?php

chdir(__DIR__);
include_once '../lib/init-cli.php';

$dir = dirname(__DIR__) . '/';

echo
    "# m h dom mon dow command\n" .
    "  0 0 *   *   *   php {$dir}scripts/daily.php\n";
