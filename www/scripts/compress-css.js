#!/usr/bin/env node

process.chdir(__dirname)
process.chdir('..')

const fs = require('fs')
const cssnano = require('cssnano')

const files = JSON.parse(fs.readFileSync('json/css-files.json'))

const source = files.map(file => (
    fs.readFileSync(file, 'utf-8')
)).join('\n')

cssnano().process(source, { from: undefined }).then(result => {
    fs.writeFileSync('compressed.css', result.css)
})
