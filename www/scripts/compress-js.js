#!/usr/bin/env node

process.chdir(__dirname)
process.chdir('..')

const fs = require('fs')
const uglify_js = require('uglify-js')

const files = JSON.parse(fs.readFileSync('json/js-files.json'))

let source = '(function () {\n'
files.forEach(file => {
    source += fs.readFileSync(file, 'utf8') + ';\n'
})
source += '\n})()'

fs.writeFileSync('combined.js', source)

const result = uglify_js.minify({
    'combined.js': source,
}, {
    warnings: true,
    sourceMap: { url: 'compressed.js.map' },
    output: { max_line_len: 1024 },
})

fs.writeFileSync('compressed.js', result.code)
fs.writeFileSync('compressed.js.map', result.map)

const warnings = result.warnings
if (warnings !== undefined) {
    console.log(warnings.filter(warning => {
        return !/Dropping duplicated definition/.test(warning)
    }))
}
